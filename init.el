;; Load bindings config
(live-load-config-file "bindings.el")


;;(defcustom dotemacs-cache-directory (concat user-emacs-directory ".cache/")
;;  "The storage location for various persistent files.")

(add-to-list 'default-frame-alist '(width . 180)) ; character
(add-to-list 'default-frame-alist '(height . 52)) ; lines


;;HELM
;;(global-set-key (kbd "C-x f") 'helm-projectile-find-file)
;;(global-set-key (kbd "C-x C-f") 'helm-find-files)
;;(global-set-key (kbd "C-x b") 'helm-projectile-switch-to-buffer)
;;(global-set-key (kbd "C-x C-r") 'helm-recentf)


(global-linum-mode)

;;BOOT SUPPORT
(add-to-list 'auto-mode-alist '("\\.boot\\'" . clojure-mode))
(add-to-list 'magic-mode-alist '(".* boot" . clojure-mode))

;;Wombat Color Themex
;;(add-to-list 'load-path "~/.live-packs/emacs-live-pack/config/color-theme-wombat")
;;(require 'color-theme-wombat)

;;Noctilux Color Themex
;(add-to-list 'load-path "~/.live-packs/emacs-live-pack/themes/noctilux-theme")
;(add-to-list 'custom-theme-load-path "~/.live-packs/emacs-live-pack/themes/noctilux-theme")
;(require 'noctilux-theme)
;(load-theme 'noctilux t)

(defun noprompt/forward-transpose-sexps ()
  (interactive)
  (paredit-forward)
  (transpose-sexps 1)
  (paredit-backward))

(defun noprompt/backward-transpose-sexps ()
  (interactive)
  (transpose-sexps 1)
  (paredit-backward)
  (paredit-backward))

(key-chord-define-global "tk" 'noprompt/forward-transpose-sexps)
(key-chord-define-global "tj" 'noprompt/backward-transpose-sexps)



(add-hook 'focus-out-hook 'save-all)

;;(set-frame-font "Source Code Pro-14" nil t)

(when (eq system-type 'darwin)
      (set-default-font "-*-Hack-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1"))

(defun cider-eval-expression-at-point-in-repl ()
  (interactive)
  (let ((form (cider-defun-at-point)))
    ;; Strip excess whitespace
    (while (string-match "\\`\s+\\|\n+\\'" form)
           (setq form (replace-match "" t t form)))
    (set-buffer (cider-get-repl-buffer))
    (goto-char (point-max))
    (insert form)
    (cider-repl-return)))

(require 'cider-mode)
(define-key cider-mode-map
            (kbd "C-;") 'cider-eval-expression-at-point-in-repl)



(defun my-run-in-nrepl (str)
  "Run a string in the repl by executing it in the current buffer.
  If output in the miloi-buffer is ok use nrepl-interactive-eval instead"
  (interactive)
  (with-current-buffer (get-buffer (cider-current-repl-buffer))
    (goto-char (point-max))
    (insert str)
    (cider-repl-return)))

(defun figwheel-connect ()
  (interactive)
  (my-run-in-nrepl
   (format "%s"
           '(do
                (require '[figwheel-sidecar.repl-api :refer :all])
                (cljs-repl)))))

(defun my-figwheel-repl ()
  (interactive)
  (cider-connect "localhost" 7888)
  (figwheel-connect))

(defun reloaded-reset ()
  (interactive)
  (my-run-in-nrepl
   (format "%s"
           '(user/reset))))

(defun reloaded-go ()
  (interactive)
  (my-run-in-nrepl
   (format "%s"
       '(user/reset))))


(setq make-backup-files nil)

(key-chord-define-global
 "DD"
 'cider-debug-defun-at-point)

(global-set-key "\M-[1;5C"    'forward-char)  ; Ctrl+right   => forward word
(global-set-key "\M-[1;5D"    'backward-char)
(global-set-key "\M-[1;5A"    'previous-line)
(global-set-key "\M-[1;5B"    'next-line)

(defun --running-as-server ()
    "Returns true if `server-start' has been called."
  (condition-case nil
      (and (boundp 'server-process)
           (memq (process-status server-process)
                 '(connect listen open run)))
    (error)))



;;(add-to-list 'load-path "~/.live-packs/emacs-live-pack/config/comment-sexp")
;;(require 'comment-sexp)

(defun top-join-line ()
  "Join the current line with the line beneath it."
  (interactive)
  (delete-indentation 1))

(defun duplicate-line (arg)
  "Duplicate current line, leaving point in lower line."
  (interactive "*p")

  ;; save the point for undo
  (setq buffer-undo-list (cons (point) buffer-undo-list))

  ;; local variables for start and end of line
  (let ((bol (save-excursion (beginning-of-line) (point)))
        eol)
    (save-excursion

      ;; don't use forward-line for this, because you would have
      ;; to check whether you are at the end of the buffer
      (end-of-line)
      (setq eol (point))

      ;; store the line and disable the recording of undo information
      (let ((line (buffer-substring bol eol))
            (buffer-undo-list t)
            (count arg))
        ;; insert the line arg times
        (while (> count 0)
          (newline)         ;; because there is no newline in 'line'
          (insert line)
          (setq count (1- count))))


      ;; create the undo information
      (setq buffer-undo-list (cons (cons eol (point)) buffer-undo-list))))
     ; end-of-let

  ;; put the point in the lowest line and return
  (next-line arg))



(defun move-line-up ()
  (interactive)
  (setq col (current-column))
  (transpose-lines 1)
  (forward-line -2)
  (forward-char col))

(defun move-line-down ()
  (interactive)
  (setq col (current-column))
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (forward-char col))
